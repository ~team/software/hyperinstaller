# A desktop environment selector.

import hyperinstaller

is_top_level_profile = True

__description__ = \
    'Provides a selection of desktop environments and ' \
    'tiling window managers (e.g. lumina, awesome , i3)'

# New way of defining packages for a profile,
# which is iterable and can be used out side of the
# profile to get a list of "what packages will be installed".
__packages__ = [
    'nano',
    'vim',
    'openssh',
    'htop',
    'wget',
    'iw',
    'wireless_tools',
    'wpa_supplicant',
    'smartmontools',
    'xdg-utils',
    ]

__supported__ = [
    'lumina',
    'awesome',
    'i3',
    ]


def _prep_function(*args, **kwargs):
    """
    Magic function called by the importing installer
    before continuing any further. It also avoids executing any
    other code in this stage. So it's a safe way to ask the user
    for more input before any other installer steps start.
    """
    hyperinstaller.print_large_list(__supported__, margin_bottom=1)
    desktop = hyperinstaller.generic_select(
        __supported__,
        allow_empty_input=False,
        input_text='Select your desired desktop environment: ',
        options_output=False,
        sort=True)

    # Temporarily store the selected desktop profile
    # in a session-safe location, since this module will get reloaded
    # the next time it gets executed.
    if not hyperinstaller.storage.get('_desktop_profile', None):
        hyperinstaller.storage['_desktop_profile'] = desktop
    if not hyperinstaller.arguments.get('desktop-environment', None):
        hyperinstaller.arguments['desktop-environment'] = desktop
    profile = hyperinstaller.Profile(None, desktop)
    # Loading the instructions with a custom namespace,
    # ensures that a __name__ comparison is never triggered.
    with profile.load_instructions(namespace=f'{desktop}.py') as imported:
        if hasattr(imported, '_prep_function'):
            return imported._prep_function()
        else:
            print('Deprecated (??): '
                  f'{desktop} profile has no _prep_function() anymore')


if __name__ == 'desktop':
    """
    This "profile" is a meta-profile.
    There are no desktop-specific steps,
    it simply routes the installer to whichever
    desktop environment/window manager was chosen.

    Maybe in the future, a network configurations or
    similar things *could* be added here.

    There are plenty of
    desktop-turn-key-solutions based on Hyperbola Project,
    this is therefore just a helper to get started
    """
    # Install common packages for all desktop environments
    hyperinstaller.storage['installation_session'].add_additional_packages(
        __packages__)

    hyperinstaller.storage['installation_session'].install_profile(
        hyperinstaller.storage['_desktop_profile'])
