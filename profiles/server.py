# Used to select various server application profiles
# on top of a minimal installation.

import logging

import hyperinstaller

is_top_level_profile = True

__description__ = \
    'Provides a selection of various server packages to ' \
    'install and enable. (e.g. apache, nginx, mariadb)'

available_servers = [
    'apache',
    'lighttpd',
    'mariadb',
    'nginx',
    'postgresql',
    'openssh',
    ]


def _prep_function(*args, **kwargs):
    """
    Magic function called by the importing installer
    before continuing any further.
    """
    if not hyperinstaller.storage.get('_selected_servers', None):
        selected_servers = hyperinstaller.generic_multi_select(
            available_servers,
            'Choose which servers to install and enable '
            '(leave blank for a minimal installation): ')
        hyperinstaller.storage['_selected_servers'] = selected_servers

        return True


if __name__ == 'server':
    """This "profile" is a meta-profile."""
    hyperinstaller.log(
        'Now installing the selected servers.',
        level=logging.INFO)
    hyperinstaller.log(
        hyperinstaller.storage['_selected_servers'],
        level=logging.DEBUG)
    for server in hyperinstaller.storage['_selected_servers']:
        hyperinstaller.log(f'Installing {server} ...', level=logging.INFO)
        app = hyperinstaller.Application(
            hyperinstaller.storage['installation_session'],
            server)
        app.install()

    hyperinstaller.log(
        'If your selections included multiple servers with the same port, '
        'you may have to reconfigure them.',
        fg='yellow',
        level=logging.INFO)
