# A desktop environment using "Lumina".

import hyperinstaller

is_top_level_profile = False

__packages__ = [
    'slim',
    'lumina',
    'lumina-extra',
    ]


def _prep_function(*args, **kwargs):
    """
    Magic function called by the importing installer
    before continuing any further. It also avoids executing any
    other code in this stage. So it's a safe way to ask the user
    for more input before any other installer steps start.
    """
    # Lumina requires a functional Xenocara installation.
    profile = hyperinstaller.Profile(None, 'xenocara')
    with profile.load_instructions(namespace='xenocara.py') as imported:
        if hasattr(imported, '_prep_function'):
            return imported._prep_function()
        else:
            print('Deprecated (??): '
                  'xenocara profile has no _prep_function() anymore')


# Ensures that this code only gets executed if executed through
# importlib.util.spec_from_file_location(
#     'lumina',
#     '/somewhere/lumina.py')
# or through conventional import lumina.
if __name__ == 'lumina':
    # Install dependency profiles.
    hyperinstaller.storage['installation_session'].install_profile('xenocara')

    # Install the Lumina packages.
    hyperinstaller.storage['installation_session'].add_additional_packages(
        __packages__)

    # Enable autostart of Slim for all users.
    hyperinstaller.storage['installation_session'].enable_service('slim')
