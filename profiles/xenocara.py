# A system with "xenocara" installed.

import hyperinstaller

is_top_level_profile = True

__description__ = \
    'Installs a minimal system as well as Xenocara and ' \
    'graphics drivers.'

__packages__ = [
    'dkms',
    'xenocara-server',
    'xenocara-xinit',
    *hyperinstaller.lib.hardware.__packages__,
    ]


def _prep_function(*args, **kwargs):
    """
    Magic function called by the importing installer
    before continuing any further. It also avoids executing any
    other code in this stage. So it's a safe way to ask the user
    for more input before any other installer steps start.
    """
    hyperinstaller.storage['gfx_driver_packages'] = \
        hyperinstaller.select_driver()

    # TODO: Add language section and/or
    #       merge it with the locale selected
    #       earlier in for instance guided.py installer.

    return True


# Ensures that this code only gets executed if executed through
# importlib.util.spec_from_file_location(
#     'xenocara',
#     '/somewhere/xenocara.py')
# or through conventional import xenocara.
if __name__ == 'xenocara':
    installation_session='installation_session'
    try:
        hyperinstaller.storage[installation_session].add_additional_packages(
            'xenocara-server xenocara-xinit '
            + ' '.join(
                hyperinstaller.storage.get('gfx_driver_packages', None)))
    except:
        # Prep didn't run, so there's no driver to install.
        hyperinstaller.storage[installation_session].add_additional_packages(
            'xenocara-server xenocara-xinit')
