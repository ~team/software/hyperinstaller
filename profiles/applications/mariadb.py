import hyperinstaller

# Define the package list in order for lib to source
# which packages will be installed by this profile
__packages__ = ['mariadb']

hyperinstaller.storage['installation_session'].add_additional_packages(
    __packages__)

hyperinstaller.lib.general.SysCommand(
    f"arch-chroot {hyperinstaller.storage['installation_session'].target} "
    "mariadb-install-db --user='mysql' --basedir='/usr' "
    "--datadir='/var/lib/mysql'")

hyperinstaller.storage['installation_session'].enable_service('mariadb')
