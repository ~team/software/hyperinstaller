from .general import SysCommand
from .exceptions import RequirementError


def find_group(name):
    """
    Finds a specific group via the package database.
    """
    grps = str(SysCommand('pacman -Sg')).split(sep='\r\n')
    if name in grps:
        return True
    else:
        return False


def find_package(name):
    """
    Finds a specific package via the package database.
    """
    pkgs = str(SysCommand('pacman -Ssq')).split(sep='\r\n')
    if name in pkgs:
        return True
    else:
        return False


def validate_package_list(packages: list):
    """
    Validates a list of given packages.
    Raises `RequirementError` if one or more packages are not found.
    """
    if invalid_packages := [
            package for package in packages
            if not find_package(package) and not find_group(package)
            ]:
        raise RequirementError(f'Invalid package names: {invalid_packages}')
