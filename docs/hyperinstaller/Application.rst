.. _hyperinstaller.Application:

hyperinstaller.Application
==========================

| This class enables access to pre-programmed application configurations.
| This is not to be confused with :ref:`hyperinstaller.Profile` which is
| for pre-programmed profiles for a wider set of installation sets.


.. autofunction:: hyperinstaller.Application
