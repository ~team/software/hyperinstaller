🚨 PR Guidelines:

# New features *(v2.2.0)*

All future work towards *`v2.2.0`* is done against `master` now.<br>
Any patch work to existing versions will have to<br>
create a new branch against the tagged versions.

# Describe your PR

If the changes has been discussed in an Issue,<br>
please tag it so that we can backtrace from the issue later on.<br>
If the PR is larger than ~20 lines,<br>
please describe it here unless described in an issue.

# Testing

Any new feature or stability improvement should be tested if possible.<br>
Please follow the test instructions at the bottom of the README or<br>
use the ISO built on each PR.
