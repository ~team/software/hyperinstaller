<!-- <img src="docs/logo.png" alt="drawing" width="200"/> -->

# HyperInstaller

HyperInstaller is a tool to generate an automated OS installer (live image).<br>
This's a fork of Arch Installer.<br>
The installer also doubles as a python library to<br>
install Hyperbola and manage services,<br>
packages and other things inside the installed system<br>
*(Usually from a live medium)*.

* hyperinstaller [documentation][DOCUMENTATION]

# Installation & Usage

    $ doas pacman -S hyperinstaller

Or simply `git clone` the repo as it has no external dependencies<br>
*(but there are optional ones)*.<br>
Or use `pip install --upgrade hyperinstaller` to use as a library.

## Running the [guided][GUIDED] installer

Assuming you are on an Hyperbola GNU/Linux-libre live image and<br>
booted into EFI mode.

    # hyperinstaller

## Running from a declarative configuration file or URL

Prequisites:
   1. Edit the [configuration file][CONFIG] according to your requirements.

Assuming you are on a Hyperbola GNU/Linux-libre live image and<br>
booted into EFI mode.

    # hyperinstaller --config <path to config file or URL>

# Help?

Submit an error here on [Hyperbola Project Issue tracker][HPIT].<br>
When doing so, attach the `/var/log/hyperinstaller/install.log`<br>
to the issue ticket. This helps us help you!

# Mission Statement

HyperbolaInstaller promises to ship a [guided installer][GUIDED]
that follows the<br>
[Hyperbola Social Contract][HSC]
as well as a library to manage services, packages and<br>
other Hyperbola Project aspects.

The guided installer will provide user-friendly options along the way,<br>
but the keyword here is options,<br>
they are optional and will never be forced upon anyone.<br>
The guided installer itself is also optional to<br>
use if so desired and not forced upon anyone.

---

HyperInstaller has one fundamental function which is to be a<br>
flexible library to manage services,<br>
packages and other aspects inside the installed system.<br>
This library is in turn used by the provided guided installer but is<br>
also for anyone who wants to script their own installations.

Therefore, HyperInstaller will try its best to not introduce any<br>
breaking changes except for major releases which may<br>
break backwards compability after notifying about such changes.

# Scripting your own installation

You could just copy [guided.py][GUIDED] as a starting point.

However, assuming you're building your own<br>
ISO and want to create an automated installation process,<br>
or you want to install virtual machines on to local disk images,<br>
here is a [mimal example][MINIMAL] of how to install using<br>
hyperinstaller as a Python library:<br>

```python
import hyperinstaller, getpass

# Select a harddrive and a disk password
harddrive = hyperinstaller.select_disk(hyperinstaller.all_disks())
disk_password = getpass.getpass(prompt='Disk password (won\'t echo): ')

# We disable safety precautions in the library that protects the partitions
harddrive.keep_partitions = False

# First, we configure the basic filesystem layout
with hyperinstaller.Filesystem(harddrive, hyperinstaller.GPT) as fs:
    # We create a filesystem layout that will use the entire drive
    # (this is a helper function, you can partition manually as well)
    fs.use_entire_disk(root_filesystem_type='btrfs')

    boot = fs.find_partition('/boot')
    root = fs.find_partition('/')

    boot.format('vfat')

    # Set the flag for encrypted to allow for encryption and then encrypt
    root.encrypted = True
    root.encrypt(password=disk_password)

with hyperinstaller.luks2(root, 'luksloop', disk_password) as unlocked_root:
    unlocked_root.format(root.filesystem)
    unlocked_root.mount('/mnt')

    boot.mount('/mnt/boot')

with hyperinstaller.Installer('/mnt') as installation:
    if installation.minimal_installation():
        installation.set_hostname('minimal-arch')
        installation.add_bootloader()

        installation.add_additional_packages(['nano', 'wget', 'git'])

        # Optionally, install a profile of choice.
        # In this case, we install a minimal profile that is empty
        installation.install_profile('minimal')

        installation.user_create('devel', 'devel')
        installation.user_set_pw('root', 'airoot')
```

This installer will perform the following:

* Prompt the user to select a disk and disk-password
* Proceed to wipe the selected disk with a `GPT` partition table on a<br>
  UEFI system and MBR on a BIOS system.
* Sets up a default 100% used disk with encryption.
* Installs a basic instance of Hyperbola GNU/Linux-libre<br>
  *(base, linux-libre-lts, kernel-firmware, grub)*
* Installs and configures a bootloader to partition 0 on uefi.<br>
  On BIOS, it sets the root to partition 0.
* Install additional packages *(nano, wget, git)*

> **Creating your own ISO with this script on it:**<br>
> Follow [HyperISO][HISO]'s guide on how to create your own ISO or<br>
> use a pre-built [guided ISO][GISO] to skip the python installation step,<br>
> or to create auto-installing ISO templates.<br>
> Further down are examples and cheat sheets on<br>
> how to create different live ISO's.

## Unattended installation based on MAC address

HyperInstaller comes with a [unattended][UNATTENDED] example which will<br>
look for a matching profile for the machine it is being run on,<br>
based on any local MAC address.<br>
For instance, if the machine that [unattended][UNATTENDED] is run on has the<br>
MAC address `52:54:00:12:34:56` it will look for a profile called<br>
[profiles/52-54-00-12-34-56.py][PROFILE_MAC].<br>
If it's found, the unattended installation will commence and<br>
source that profile as it's installation procedure.

# Testing

## Using a Live ISO Image

If you want to test a commit, branch or bleeding edge release from the<br>
repository using the vanilla Hyperbola Live ISO image,<br>
you can replace the version of hyperinstaller with a new version and<br>
run that with the steps described below:

1. You need a working network connection
2. Install the build requirements with<br>
   `pacman -Sy; pacman -S git python-pip`<br>
   *(note that this may or may not work depending on your RAM and<br>
   current state of the squashfs maximum filesystem free space)*
3. Uninstall the previous version of hyperinstaller with<br>
   `pip uninstall hyperinstaller`
4. Now clone the latest repository with<br>
   `git clone https://git.hyperbola.info:50100/software/hyperinstaller.git/`
5. Enter the repository with `cd hyperinstaller`<br>
   *At this stage, you can choose to check out a feature branch for<br>
   instance with `git checkout v2.2.0`*
6. Build the project and install it using `python setup.py install`

After this, running hyperinstaller with `python -m hyperinstaller` will<br>
run against whatever branch you chose in step 5.

## Without a Live ISO Image

To test this without a live ISO,<br>
the simplest approach is to use a local image and create a loop device.<br>
This can be done by installing `pacman -S arch-install-scripts util-linux`<br>
locally and doing the following:

    # truncate -s 20G testimage.img
    # losetup -fP ./testimage.img
    # losetup -a | grep "testimage.img" | awk -F ":" '{print $1}'
    # pip install --upgrade hyperinstaller
    # python -m hyperinstaller --script guided
    # qemu-system-x86_64 -enable-kvm -machine q35,accel=kvm \
      -device intel-iommu -cpu host -m 4096 -boot order=d \
      -drive file=./testimage.img,format=raw \
      -drive if=pflash,format=raw,readonly,file=/usr/share/ovmf/x64/OVMF_CODE.fd \
      -drive if=pflash,format=raw,readonly,file=/usr/share/ovmf/x64/OVMF_VARS.fd

This will create a *20 GB* `testimage.img` and create a<br>
loop device which we can use to format and install to.<br>
`hyperinstaller` is installed and executed in [guided mode][GUIDED_TODO].<br>
Once the installation is complete,<br>
~~you can use qemu/kvm to boot the test media.~~<br>
*(You'd actually need to do some EFI magic in order to point the<br>
EFI vars to the partition 0 in the test medium,<br>
so this won't work entirely out of the box,<br>
but that gives you a general idea of what we're going for here)*

There's also a [Building and Testing][B+T] guide.<br>
It will go through everything from packaging,<br>
building and running *(with qemu)* the installer against a dev branch.



[DOCUMENTATION]: https://wiki.hyperbola.info
[CONFIG]: examples/config-sample.json
[HPIT]: https://issues.hyperbola.info
[GUIDED]: examples/guided.py
[MINIMAL]: examples/minimal.py
[HISO]: https://git.hyperbola.info:50100/software/hyperiso.git/
[GISO]: https://wiki.hyperbola.info
[UNATTENDED]: examples/unattended.py
[PROFILE_MAC]: profiles/52-54-00-12-34-56.py
[GUIDED_TODO]: #docs-todo
[HSC]: https://wiki.hyperbola.info/doku.php?id=en:main:social_contract
[B+T]: https://wiki.hyperbola.info
