import hyperinstaller

# Select a harddrive and a disk password
hyperinstaller.log('Minimal only supports:\n'
                   ' * Being installed to a single disk')

if hyperinstaller.arguments.get('help', None):
    hyperinstaller.log(' - Optional disk encryption via '
                       '--!encryption-password=<password>\n'
                       ' - Optional filesystem type '
                       'via --filesystem=<fs type>\n'
                       ' - Optional network via --network')

hyperinstaller.arguments['harddrive'] = \
    hyperinstaller.select_disk(hyperinstaller.all_disks())


def install_on(mountpoint):
    # We kick off the installer by telling it where the
    with hyperinstaller.Installer(mountpoint) as installation:
        # Strap in the base system, add a boot loader and configure
        # some other minor details as
        # specified by this profile and user.
        if installation.minimal_installation():
            installation.set_hostname('minimal-hyperbola')
            installation.add_bootloader()

            # Optionally enable networking:
            if hyperinstaller.arguments.get('network', None):
                installation.copy_live_network_config(enable_services=True)

            installation.add_additional_packages(['nano', 'wget', 'git'])
            installation.install_profile('minimal')

            installation.user_create('devel', 'devel')
            installation.user_set_pw('root', 'airoot')

    # Once this is done, we output some useful information to the user
    # And the installation is complete.
    hyperinstaller.log('There are two new accounts in '
                       'your installation after reboot:\n'
                       ' * root (password: airoot)\n'
                       ' * devel (password: devel)')


if hyperinstaller.arguments['harddrive']:
    hyperinstaller.arguments['harddrive'].keep_partitions = False

    print(f" ! Formatting {hyperinstaller.arguments['harddrive']} in ",
          end='')
    hyperinstaller.do_countdown()

    # First, we configure the basic filesystem layout
    with hyperinstaller.Filesystem(
            hyperinstaller.arguments['harddrive'],
            hyperinstaller.GPT) as fs:
        # We use the entire disk instead of
        # setting up partitions on your own
        if hyperinstaller.arguments['harddrive'].keep_partitions is False:
            fs.use_entire_disk(
                root_filesystem_type=hyperinstaller.arguments.get(
                    'filesystem',
                    'btrfs'))

        boot = fs.find_partition('/boot')
        root = fs.find_partition('/')

        boot.format('fat32')

        # We encrypt the root partition if
        # we got a password to do so with,
        # Otherwise we just skip straight to
        # formatting and installation
        if hyperinstaller.arguments.get('!encryption-password', None):
            root.encrypted = True
            root.encrypt(
                password=hyperinstaller.arguments.get(
                    '!encryption-password',
                    None))

            with hyperinstaller.luks2(
                    root,
                    'luksloop',
                    hyperinstaller.arguments.get(
                        '!encryption-password',
                        None)) as unlocked_root:
                unlocked_root.format(root.filesystem)
                unlocked_root.mount('/mnt')
        else:
            root.format(root.filesystem)
            root.mount('/mnt')

        boot.mount('/mnt/boot')

install_on('/mnt')
