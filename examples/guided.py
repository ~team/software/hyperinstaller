import json
import logging
import os
import pathlib
import time

import hyperinstaller
from hyperinstaller.lib.general import run_custom_user_commands
from hyperinstaller.lib.hardware import *
from hyperinstaller.lib.networking import check_mirror_reachable
from hyperinstaller.lib.profiles import Profile, is_desktop_profile

if hyperinstaller.arguments.get('help'):
    print('See `man hyperinstaller` for help.')
    exit(0)
if os.getuid() != 0:
    print('HyperInstaller requires root privileges to run. '
          'See --help for more.')
    exit(1)

# Log various information about
# hardware before starting the installation.
# This might assist in troubleshooting
hyperinstaller.log(
    f'Hardware model detected: {hyperinstaller.sys_vendor()} '
    f'{hyperinstaller.product_name()}; '
    f'UEFI mode: {hyperinstaller.has_uefi()}\n'
    f'Processor model detected: {hyperinstaller.cpu_model()}\n'
    f'Memory statistics: {hyperinstaller.mem_available()} available out of '
    f'{hyperinstaller.mem_total()} total installed\n'
    f'Graphics devices detected: {hyperinstaller.graphics_devices().keys()}',
    level=logging.DEBUG)

# For support reasons,
# we'll log the disk layout pre installation to
# match against post-installation layout
hyperinstaller.log(
    f'Disk states before installing: {hyperinstaller.disk_layouts()}',
    level=logging.DEBUG)


def load_config():
    if hyperinstaller.arguments.get('harddrives', None) is not None:
        if type(hyperinstaller.arguments['harddrives']) is str:
            hyperinstaller.arguments['harddrives'] = \
                hyperinstaller.arguments['harddrives'].split(',')

        hyperinstaller.arguments['harddrives'] = [
            hyperinstaller.BlockDevice(BlockDev) for BlockDev in
            hyperinstaller.arguments['harddrives']
            ]
        # Temporarily disabling keep_partitions if
        # config file is loaded
        # Temporary workaround to make Desktop Environments work

    if hyperinstaller.arguments.get('profile', None) is not None:
        if type(hyperinstaller.arguments.get('profile', None)) is dict:
            hyperinstaller.arguments['profile'] = hyperinstaller.Profile(
                None,
                hyperinstaller.arguments.get('profile', None)['path'])
        else:
            hyperinstaller.arguments['profile'] = hyperinstaller.Profile(
                None,
                hyperinstaller.arguments.get('profile', None))

    hyperinstaller.storage['_desktop_profile'] = \
        hyperinstaller.arguments.get('desktop-environment', None)
    if hyperinstaller.arguments.get('mirror-region', None) is not None:
        if type(hyperinstaller.arguments.get('mirror-region', None)) is dict:
            hyperinstaller.arguments['mirror-region'] = \
                hyperinstaller.arguments.get('mirror-region', None)
        else:
            selected_region = \
                hyperinstaller.arguments.get('mirror-region', None)
            hyperinstaller.arguments['mirror-region'] = {
                selected_region:
                hyperinstaller.list_mirrors()[selected_region]
                }

    if hyperinstaller.arguments.get('sys-language', None) is not None:
        hyperinstaller.arguments['sys-language'] = \
            hyperinstaller.arguments.get('sys-language', 'en_US')
    if hyperinstaller.arguments.get('sys-encoding', None) is not None:
        hyperinstaller.arguments['sys-encoding'] = \
            hyperinstaller.arguments.get('sys-encoding', 'utf-8')
    if hyperinstaller.arguments.get('gfx_driver', None) is not None:
        hyperinstaller.storage['gfx_driver_packages'] = \
            AVAILABLE_GFX_DRIVERS.get(
                hyperinstaller.arguments.get('gfx_driver', None),
                None)
    if hyperinstaller.arguments.get('servers', None) is not None:
        hyperinstaller.storage['_selected_servers'] = \
            hyperinstaller.arguments.get('servers', None)
    if hyperinstaller.arguments.get('disk_layouts', None) is not None:
        dl_path = pathlib.Path(hyperinstaller.arguments['disk_layouts'])
        if dl_path.exists() and str(dl_path).endswith('.json'):
            try:
                with open(dl_path) as fh:
                    hyperinstaller.storage['disk_layouts'] = json.load(fh)
            except Exception as e:
                raise ValueError('--disk_layouts does not contain a '
                                 f'valid JSON format: {e}')
        else:
            try:
                hyperinstaller.storage['disk_layouts'] = \
                    json.loads(hyperinstaller.arguments['disk_layouts'])
            except:
                raise ValueError(
                    '--disk_layouts=<json> needs either a JSON file or '
                    'a JSON string given with a valid disk layout.')


def ask_user_questions():
    """
    First, we will ask the user for a bunch of user input.
    Not until we are satisfied with what we want to install
    will we continue with the actual installation steps.
    """
    if not hyperinstaller.arguments.get('keyboard-layout', None):
        while True:
            try:
                hyperinstaller.arguments['keyboard-layout'] = \
                    hyperinstaller.select_language(
                        hyperinstaller.list_cui_keyboard_languages()).strip()
                break
            except hyperinstaller.RequirementError as err:
                hyperinstaller.log(err, fg='red')

    # Before continuing, set the
    # preferred keyboard layout/language in the current terminal.
    # This will just help the user with the next following questions.
    if len(hyperinstaller.arguments['keyboard-layout']):
        hyperinstaller.set_cui_keyboard_language(
            hyperinstaller.arguments['keyboard-layout'])

    # Set which region to download packages from
    # during the installation
    if not hyperinstaller.arguments.get('mirror-region', None):
        while True:
            try:
                hyperinstaller.arguments['mirror-region'] = \
                    hyperinstaller.select_mirror_regions(
                        hyperinstaller.list_mirrors())
                break
            except hyperinstaller.RequirementError as e:
                hyperinstaller.log(e, fg='red')

    if not hyperinstaller.arguments.get('sys-language', None) and \
            hyperinstaller.arguments.get('advanced', False):
        hyperinstaller.arguments['sys-language'] = input(
            'Enter a valid locale (language) for your OS, '
            '(Default: en_US): ').strip()
        hyperinstaller.arguments['sys-encoding'] = input(
            'Enter a valid system default encoding for your OS, '
            '(Default: utf-8): ').strip()
        hyperinstaller.log('Keep in mind that if you want multiple locales, '
                           'post configuration is required.',
                           fg='yellow')

    if not hyperinstaller.arguments.get('sys-language', None):
        hyperinstaller.arguments['sys-language'] = 'en_US'
    if not hyperinstaller.arguments.get('sys-encoding', None):
        hyperinstaller.arguments['sys-encoding'] = 'utf-8'

    # Ask which harddrives/block-devices we will install to
    # and convert them into hyperinstaller.BlockDevice() objects.
    if hyperinstaller.arguments.get('harddrives', None) is None:
        hyperinstaller.arguments['harddrives'] = \
            hyperinstaller.generic_multi_select(
                hyperinstaller.all_disks(),
                text=' -- Leave blank when done. --\n'
                'Select one or more harddrives to use and configure: ',
                allow_empty=False)

    if hyperinstaller.arguments.get('harddrives', None) is not None and \
            hyperinstaller.storage.get('disk_layouts', None) is None:
        hyperinstaller.storage['disk_layouts'] = \
            hyperinstaller.select_disk_layout(
                hyperinstaller.arguments['harddrives'])

    # Get disk encryption password (or skip if blank)
    #if hyperinstaller.arguments['harddrives'] and \
    #        hyperinstaller.arguments.get('!encryption-password', None) is \
    #        None:
    #    if (passwd := hyperinstaller.get_password(
    #            prompt=
    #                'Enter disk encryption password '
    #                '(leave blank for no encryption): ')):
    #        hyperinstaller.arguments['!encryption-password'] = passwd

    #if hyperinstaller.arguments['harddrives'] and \
    #        hyperinstaller.arguments.get('!encryption-password', None):
    #    # If no partitions was marked as encrypted,
    #    # but a password was supplied and
    #    # we have some disks to format..
    #    # Then we need to identify which partitions to encrypt.
    #    # This will default to / (root).
    #    if len(list(hyperinstaller.encrypted_partitions(
    #            hyperinstaller.storage['disk_layouts']))) == 0:
    #        hyperinstaller.storage['disk_layouts'] = \
    #            hyperinstaller.select_encrypted_partitions(
    #                hyperinstaller.storage['disk_layouts'],
    #                hyperinstaller.arguments['!encryption-password'])

    # Ask about init system selection if one is not already set
    if not hyperinstaller.arguments.get('init', None):
        hyperinstaller.arguments['init'] = \
            hyperinstaller.ask_for_init_selection()

    # Ask about bootloader selection if one is not already set
    if not hyperinstaller.arguments.get('bootloader', None):
        hyperinstaller.arguments['bootloader'] = \
            hyperinstaller.ask_for_bootloader_selection()

    # Get the hostname for the machine
    if not hyperinstaller.arguments.get('hostname', None):
        hyperinstaller.arguments['hostname'] = input(
            'Desired hostname for the installation '
            '(leave blank to set localhost): ').strip(' ')

    # Ask for a root password
    # (optional, but triggers requirement for super-user if skipped)
    if not hyperinstaller.arguments.get('!root-password', None):
        hyperinstaller.arguments['!root-password'] = \
            hyperinstaller.get_password(
                prompt=
                    'Enter root password (Recommendation: '
                    'leave blank to leave root disabled): ')

    # Ask for additional users (super-user if root pw was not set)
    if not hyperinstaller.arguments.get('!root-password', None) and \
            not hyperinstaller.arguments.get('superusers', None):
        hyperinstaller.arguments['superusers'] = \
            hyperinstaller.ask_for_superuser_account(
                'Create a required super-user with doas privileges: ',
                forced=True)
        users, superusers = hyperinstaller.ask_for_additional_users(
            'Enter a username to create a additional user '
            '(leave blank to skip & continue): ')
        hyperinstaller.arguments['users'] = users
        hyperinstaller.arguments['superusers'] = {
            **hyperinstaller.arguments['superusers'],
            **superusers
            }

    # Ask for hyperinstaller-specific profiles
    # (such as desktop environments etc)
    if not hyperinstaller.arguments.get('profile', None):
        hyperinstaller.arguments['profile'] = hyperinstaller.select_profile()

	# Check the potentially selected profiles preparations to
        # get early checks if some additional questions are needed.
    if hyperinstaller.arguments['profile'] and \
            hyperinstaller.arguments['profile'].has_prep_function():
        with hyperinstaller.arguments['profile'].load_instructions(
                namespace=
                    hyperinstaller.arguments['profile'].namespace
                    + '.py') as imported:
            if not imported._prep_function():
                hyperinstaller.log(' * Profile is preparation requirements '
                                   'was not fulfilled.',
                                   fg='red')
                exit(1)

    # Ask about sound server selection if one is not already set
    if not hyperinstaller.arguments.get('sound', None):
        # The argument to ask_for_sound_selection lets the
        # library know if it's a desktop profile
        hyperinstaller.arguments['sound'] = \
            hyperinstaller.ask_for_sound_selection(
                is_desktop_profile(hyperinstaller.arguments['profile']))

    # Ask for preferred kernel:
    if not hyperinstaller.arguments.get('kernels', None):
        kernels = ['linux-libre-lts']
        hyperinstaller.arguments['kernels'] = \
            hyperinstaller.select_kernel(kernels)

    # Ask about developer tools installation
    hyperinstaller.arguments['devel'] = \
        input('Would you like to install developer tools? '
              '[y/N] ').strip().lower() in ('y', 'yes')

    # Additional packages
    # (with some light weight error handling for invalid package names)
    print(" -- Only packages such as 'base', 'linux-libre-lts', "
          "'kernel-firmware', 'grub' "
          'and optional profile packages are installed. --\n'
          ' -- If you desire a web browser, such as '
          "'iceweasel-uxp', 'iceape-uxp', 'midori' "
          'or another software package; --\n'
          ' -- you may specify it in the following prompt '
          'and separate packages with spaces. --\n'
          ' -- You can skip this step by leaving the option blank --')
    while True:
        if not hyperinstaller.arguments.get('packages', None):
            hyperinstaller.arguments['packages'] = [
                package for package in input(
                    'Write additional packages to install: ').split(
                        ' ') if len(package)]

        if len(hyperinstaller.arguments['packages']):
            # Verify packages that were given
            try:
                hyperinstaller.log('Verifying that additional packages exist '
                                   '(this might take a few seconds)')
                hyperinstaller.validate_package_list(
                    hyperinstaller.arguments['packages'])
                break
            except hyperinstaller.RequirementError as e:
                hyperinstaller.log(e, fg='red')
                hyperinstaller.arguments['packages'] = None
                # Clear the packages to trigger a new input question
        else:
            # no additional packages were selected, which we'll allow
            break

    # Ask or Call the helper function that asks the user to
    # optionally configure a network.
    if not hyperinstaller.arguments.get('nic', None):
        hyperinstaller.arguments['nic'] = \
            hyperinstaller.ask_to_configure_network()
        if not hyperinstaller.arguments['nic']:
            hyperinstaller.log('No network configuration was selected. '
                               'Network is going to be unavailable until '
                               'configured manually!',
                               fg='yellow')

    if not hyperinstaller.arguments.get('timezone', None):
        hyperinstaller.arguments['timezone'] = \
            hyperinstaller.ask_for_a_timezone()

    if hyperinstaller.arguments['timezone']:
        if not hyperinstaller.arguments.get('ntp', False):
            hyperinstaller.arguments['ntp'] = input(
                'Would you like to use '
                'automatic time synchronization (NTP) '
                'with the default time servers? [Y/n]: ').strip().lower() in (
                    'y',
                    'yes',
                    '')
            if hyperinstaller.arguments['ntp']:
                hyperinstaller.log(
                    'Hardware time and other post-configuration steps '
                    'might be required in order for NTP to work. '
                    'For more information, please check the Arch wiki.',
                    fg='yellow')


def perform_filesystem_operations():
    print('\nThis is your chosen configuration:')
    hyperinstaller.log('-- Guided template chosen (with below config) --',
                       level=logging.DEBUG)
    user_configuration = json.dumps(hyperinstaller.arguments,
                                    indent=4,
                                    sort_keys=True,
                                    cls=hyperinstaller.JSON)
    hyperinstaller.log(user_configuration, level=logging.INFO)
    with open('/var/log/hyperinstaller/user_configuration.json', 'w') as \
            config_file:
        config_file.write(user_configuration)
    user_disk_layout = json.dumps(hyperinstaller.storage['disk_layouts'],
                                  indent=4,
                                  sort_keys=True,
                                  cls=hyperinstaller.JSON)
    hyperinstaller.log(user_disk_layout, level=logging.INFO)
    with open('/var/log/hyperinstaller/user_disk_layout.json', 'w') as \
            disk_layout_file:
        disk_layout_file.write(user_disk_layout)
    print()

    if hyperinstaller.arguments.get('dry-run'):
        exit(0)

    if not hyperinstaller.arguments.get('silent'):
        input('Press Enter to continue.')

    """
    Issue a final warning before we
    continue with something un-revertable.
    We mention the drive one last time,
    and count from 5 to 0.
    """

    if hyperinstaller.arguments.get('harddrives', None):
        print(f" ! Formatting {hyperinstaller.arguments['harddrives']} in ",
              end='')
        hyperinstaller.do_countdown()

        """
        Setup the blockdevice, filesystem (and optionally encryption).
        Once that is done, we will hand over to perform_installation()
        """
        mode = hyperinstaller.GPT
        if has_uefi() is False:
            mode = hyperinstaller.MBR

        for drive in hyperinstaller.arguments['harddrives']:
            with hyperinstaller.Filesystem(drive, mode) as fs:
                fs.load_layout(
                    hyperinstaller.storage['disk_layouts'][drive.path])


def perform_installation(mountpoint):
    """
    Performs the installation steps on a block device.
    Only requirement is that the block devices are
    formatted and setup prior to entering this function.
    """
    with hyperinstaller.Installer(
            mountpoint,
            kernels=hyperinstaller.arguments.get('kernels', 'linux')) as \
                installation:
        # Mount all the drives to the desired mountpoint
        # This *can* be done outside of the installation,
        # but the installer can deal with it.
        installation.mount_ordered_layout(
            hyperinstaller.storage['disk_layouts'])

        # Set mirrors used by pacstrap (outside of installation)
        if hyperinstaller.arguments.get('mirror-region', None):
            hyperinstaller.use_mirrors(
                hyperinstaller.arguments['mirror-region'])
                # Set the mirrors for the live medium
        if installation.minimal_installation():
            installation.set_locale(
                hyperinstaller.arguments['sys-language'],
                hyperinstaller.arguments['sys-encoding'].upper())
            installation.set_hostname(hyperinstaller.arguments['hostname'])
            if hyperinstaller.arguments['mirror-region'].get(
                    'mirrors',
                    None) is not None:
                installation.set_mirrors(
                    hyperinstaller.arguments['mirror-region'])
                # Set the mirrors in the installation medium
            installation.add_bootloader(
                hyperinstaller.arguments['bootloader'])

            # If user selected to copy the
            # current live network configuration
            # Perform a copy of the config
            if hyperinstaller.arguments.get('nic', {}) == \
                    'Copy live network configuration to installation.':
                installation.copy_live_network_config(enable_services=True)
                # Sources the live network configuration to the
                # install medium.
            # Otherwise, if a interface was selected,
            # configure that interface
            elif hyperinstaller.arguments.get('nic', {}):
                installation.configure_nic(
                    **hyperinstaller.arguments.get('nic', {}))

            if hyperinstaller.arguments.get('sound', None) is not None:
                installation.log(
                    'This sound server will be used: '
                    + hyperinstaller.arguments.get('sound', None),
                    level=logging.INFO)
                if hyperinstaller.arguments.get('sound', None) == 'sndio':
                    print('Installing sndio ...')
                    installation.add_additional_packages(
                        ['sndio', 'alsa-sndio', 'aucatctl'])
            else:
                installation.log('No sound server will be installed.',
                                 level=logging.INFO)

            if hyperinstaller.arguments.get('devel', False):
                installation.log('Installing developer tools ...',
                                 level=logging.INFO)
                installation.add_additional_packages(['base-devel'])
            else:
                installation.log('No developer tools will be installed.',
                                 level=logging.INFO)

            if hyperinstaller.arguments.get('packages', None) and \
                    hyperinstaller.arguments.get('packages', None)[0] != '':
                installation.add_additional_packages(
                    hyperinstaller.arguments.get('packages', None))

            if hyperinstaller.arguments.get('profile', None):
                installation.install_profile(
                    hyperinstaller.arguments.get('profile', None))

            for user, user_info in \
                    hyperinstaller.arguments.get('users', {}).items():
                installation.user_create(
                    user,
                    user_info['!password'],
                    doas=False)

            for superuser, user_info in \
                    hyperinstaller.arguments.get('superusers', {}).items():
                installation.user_create(
                    superuser,
                    user_info['!password'],
                    doas=True)

            if timezone := hyperinstaller.arguments.get('timezone', None):
                installation.set_timezone(timezone)

            if hyperinstaller.arguments.get('ntp', False):
                installation.activate_ntp()

            if (root_pw :=
                    hyperinstaller.arguments.get('!root-password',None)) and \
                    len(root_pw):
                installation.user_set_pw('root', root_pw)

            # This step must be after profile installs to
            # allow profiles to install language pre-requisits.
            # After which, this step will set the language both for
            # console and x11 if x11 was installed for instance.
            installation.set_cui_keyboard_language(
                hyperinstaller.arguments['keyboard-layout'])

            if hyperinstaller.arguments['profile'] and \
                    hyperinstaller.arguments['profile'].has_post_install():
                with hyperinstaller.arguments['profile'].load_instructions(
                        namespace=
                            hyperinstaller.arguments['profile'].namespace
                            + '.py') as imported:
                    if not imported._post_install():
                        hyperinstaller.log(
                            ' * Profile is post configuration requirements '
                            'was not fulfilled.',
                            fg='red')
                        exit(1)

        # If the user provided a list of services to be enabled,
        # pass the list to the enable_service function.
        # Note that while it's called enable_service,
        # it can actually take a list of services and iterate it.
        if hyperinstaller.arguments.get('services', None):
            installation.enable_service(*hyperinstaller.arguments['services'])

        if hyperinstaller.arguments.get('sound', None) == 'sndio':
             installation.config_sndio()

        # If the user provided custom commands to
        # be run post-installation,
        # execute them now.
        if hyperinstaller.arguments.get('custom-commands', None):
            run_custom_user_commands(
                hyperinstaller.arguments['custom-commands'],
                installation)

        installation.log('For post-installation tips, '
                         'see https://wiki.hyperbola.org/',
                         fg='yellow')
        if not hyperinstaller.arguments.get('silent'):
            choice = input('Would you like to chroot into the '
                           'newly created installation and '
                           'perform post-installation configuration? [Y/n] ')
            if choice.strip().lower() in ('y', 'yes', ''):
                try:
                    installation.drop_to_shell()
                except:
                    pass

    # For support reasons, we'll log the disk layout post installation
    # (crash or no crash)
    hyperinstaller.log(
        f'Disk states after installing: {hyperinstaller.disk_layouts()}',
        level=logging.DEBUG)


if not check_mirror_reachable():
    log_file = os.path.join(hyperinstaller.storage.get('LOG_PATH', None),
                            hyperinstaller.storage.get('LOG_FILE', None))
    hyperinstaller.log('Hyperbola GNU/Linux-libre mirrors are not reachable. '
                       'Please check your internet connection and '
                       f"the log file '{log_file}'.",
                       level=logging.INFO,
                       fg='red')
    exit(1)

load_config()
if not hyperinstaller.arguments.get('silent'):
    ask_user_questions()

perform_filesystem_operations()
perform_installation(hyperinstaller.storage.get('MOUNT_POINT', '/mnt'))
